-- phpMyAdmin SQL Dump
-- version 2.11.8.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 22, 2008 at 10:34 AM
-- Server version: 5.0.51
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `domainhound`
--
DROP DATABASE `domainhound`;
CREATE DATABASE `domainhound` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `domainhound`;

-- --------------------------------------------------------

--
-- Table structure for table `CONFIG`
--
-- Creation: Sep 04, 2008 at 04:02 PM
-- Last update: Sep 04, 2008 at 04:03 PM
--

DROP TABLE IF EXISTS `CONFIG`;
CREATE TABLE IF NOT EXISTS `CONFIG` (
  `maxPasswordAge` int(11) NOT NULL COMMENT 'number of days'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `CONFIG`
--

INSERT INTO `CONFIG` (`maxPasswordAge`) VALUES
(60);

-- --------------------------------------------------------

--
-- Table structure for table `DOMAINS`
--
-- Creation: Aug 31, 2008 at 11:31 PM
-- Last update: Sep 03, 2008 at 11:45 AM
--

DROP TABLE IF EXISTS `DOMAINS`;
CREATE TABLE IF NOT EXISTS `DOMAINS` (
  `domainId` int(10) unsigned NOT NULL auto_increment,
  `domainName` varchar(50) collate utf8_unicode_ci NOT NULL,
  `registrant` varchar(60) collate utf8_unicode_ci NOT NULL,
  `whenCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `whenDeleted` timestamp NULL default NULL,
  `ipAddress` tinytext collate utf8_unicode_ci NOT NULL,
  `hardware` text collate utf8_unicode_ci,
  `dateRegistered` date default NULL COMMENT 'date domain first registered',
  `dateUpdated` date default NULL COMMENT 'date domain registration updated',
  `dateExpires` date default NULL COMMENT 'date domain registration expires',
  `custom1` text collate utf8_unicode_ci,
  `custom2` text collate utf8_unicode_ci,
  `custom3` text collate utf8_unicode_ci,
  `custom4` text collate utf8_unicode_ci,
  `customName1` varchar(20) collate utf8_unicode_ci default NULL,
  `customName2` varchar(20) collate utf8_unicode_ci default NULL,
  `customName3` varchar(20) collate utf8_unicode_ci default NULL,
  `customName4` varchar(20) collate utf8_unicode_ci default NULL,
  `groupId` int(11) default NULL,
  PRIMARY KEY  (`domainId`),
  KEY `groupFK` (`groupId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `DOMAINS`
--

INSERT INTO `DOMAINS` (`domainId`, `domainName`, `registrant`, `whenCreated`, `whenDeleted`, `ipAddress`, `hardware`, `dateRegistered`, `dateUpdated`, `dateExpires`, `custom1`, `custom2`, `custom3`, `custom4`, `customName1`, `customName2`, `customName3`, `customName4`, `groupId`) VALUES
(1, 'example1.com', 'Example Registrant', '2008-08-26 19:52:04', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(3, 'example3.org', 'Sample Registrant', '2008-08-26 19:54:01', '2008-09-03 11:45:02', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2),
(4, 'example4.com', 'Reg1', '2008-08-31 01:04:10', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 3),
(5, 'exadsfafdample5.com', 'Reg1adfasdf', '2008-08-31 23:40:30', NULL, '157.02.2.1', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 4),
(6, 'example6.com', 'example6', '2008-09-01 00:21:52', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 6),
(7, '', 'reg2', '2008-09-01 00:21:52', '2008-09-01 07:22:22', '120.4.7.30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 7),
(8, 'example8.com', 'Example Registrant8', '2008-09-01 00:39:39', NULL, '120.4.7.30  	', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'poo.com', '', '2008-09-01 03:45:01', NULL, '', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(10, 'domainhound.com', 'group7', '2008-09-01 03:58:38', NULL, '123.456.789.254', 'Cray', '0000-00-00', '0000-00-00', '0000-00-00', 'Linux', 'Beagle', '', '', 'OS', 'Breed', '', '', NULL),
(11, 'ucsd.com', 'g7', '2008-09-01 04:01:13', NULL, '255.255.255.255', 'Enigma machine', '1999-09-01', '2000-09-10', '2012-12-12', 'Solaris', 'Computer Science', '', '', 'OS', 'Major', '', '', NULL),
(12, 'abc.com', '', '2008-09-01 04:26:47', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'abc.com', '', '2008-09-01 04:27:00', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'abc.com', '', '2008-09-01 04:27:03', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'abc.com', '', '2008-09-01 04:27:03', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'abc.com', '', '2008-09-01 04:27:03', '2008-09-01 06:46:35', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'abc.com', '', '2008-09-01 04:27:04', '2008-09-01 06:46:52', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'abc.com', '', '2008-09-01 04:27:04', '2008-09-01 06:48:53', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 'abc.com', '', '2008-09-01 04:27:04', '2008-09-01 06:49:38', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 'abc.com', '', '2008-09-01 04:27:04', '2008-09-01 06:49:41', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 'abc.com', '', '2008-09-01 04:27:05', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 'abc.com', '', '2008-09-01 04:27:09', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 'abc.com', '', '2008-09-01 04:27:10', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 'abc.com', '', '2008-09-01 04:27:11', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 'abc.com', '', '2008-09-01 04:27:11', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 'abc.com', '', '2008-09-01 04:27:11', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 'abc.com', '', '2008-09-01 04:27:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 'abc.com', '', '2008-09-01 04:27:12', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 'xyz.com', '', '2008-09-01 04:38:02', NULL, '157.130.59.70', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(30, 'xxx.com', '', '2008-09-01 04:38:37', NULL, '999.0.0.0', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(31, 'cats', '', '2008-09-01 04:51:30', NULL, '157.130.59.70', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(32, 'adsf=+/?k''":%(', '', '2008-09-01 04:52:38', NULL, '157.130.59.70', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(33, 'zzy.com', '', '2008-09-01 05:03:50', NULL, '157.130.59.70', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(34, 'asdf', '', '2008-09-01 05:04:24', NULL, '157.130.59.70', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL),
(35, 'x-y', '', '2008-09-01 05:05:00', NULL, '157.130.59.70', '', '0000-00-00', '0000-00-00', '0000-00-00', '', '', '', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `GROUPS`
--
-- Creation: Aug 26, 2008 at 10:52 AM
-- Last update: Sep 03, 2008 at 11:51 AM
--

DROP TABLE IF EXISTS `GROUPS`;
CREATE TABLE IF NOT EXISTS `GROUPS` (
  `groupId` int(10) unsigned NOT NULL auto_increment,
  `groupName` varchar(60) collate utf8_unicode_ci default NULL,
  `domainIds` text collate utf8_unicode_ci COMMENT 'list of domain ids in this group, space separated',
  `groupIdParent` int(11) default NULL,
  `whenCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `whenDeleted` timestamp NULL default NULL,
  PRIMARY KEY  (`groupId`),
  KEY `groupFK` (`groupIdParent`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `GROUPS`
--

INSERT INTO `GROUPS` (`groupId`, `groupName`, `domainIds`, `groupIdParent`, `whenCreated`, `whenDeleted`) VALUES
(1, 'boogers', ' 1 4 5 6', 7, '2008-08-22 22:53:03', NULL),
(3, 'green', NULL, 0, '2008-08-26 13:29:26', NULL),
(4, 'blue', NULL, 0, '2008-08-26 13:29:26', NULL),
(5, 'yellow', ' 3 6', 0, '2008-08-26 20:06:30', NULL),
(6, 'orange', ' 4', 3, '2008-08-26 20:45:35', NULL),
(7, 'purple', ' 4', 10, '2008-08-26 23:11:52', '2008-09-01 07:05:03'),
(10, 'pink', '3', 1, '2008-08-29 13:45:32', NULL),
(11, 'brown', NULL, NULL, '2008-08-29 13:45:54', '2008-09-01 06:20:46'),
(13, 'lime', NULL, NULL, '2008-08-29 13:56:54', NULL),
(14, 'lemon', ' 4', 0, '2008-08-29 14:00:38', NULL),
(15, 'cats', NULL, NULL, '2008-08-29 14:38:41', '2008-09-01 06:22:30'),
(16, 'dogs', NULL, NULL, '2008-08-29 17:47:16', NULL),
(17, 'none', NULL, NULL, '2008-09-03 11:51:32', '2008-09-03 11:51:39');

-- --------------------------------------------------------

--
-- Table structure for table `USERS`
--
-- Creation: Aug 30, 2008 at 09:50 PM
-- Last update: Sep 10, 2008 at 05:24 PM
--

DROP TABLE IF EXISTS `USERS`;
CREATE TABLE IF NOT EXISTS `USERS` (
  `userId` int(10) unsigned NOT NULL auto_increment,
  `userName` varchar(35) collate utf8_unicode_ci NOT NULL,
  `password` binary(64) NOT NULL COMMENT 'sha512 of password+salt',
  `accessLevel` varchar(20) collate utf8_unicode_ci NOT NULL,
  `firstName` varchar(35) collate utf8_unicode_ci default NULL,
  `lastName` varchar(35) collate utf8_unicode_ci default NULL,
  `email` varchar(50) collate utf8_unicode_ci default NULL,
  `whenLastLogin` timestamp NULL default NULL,
  `whenCreated` timestamp NOT NULL default CURRENT_TIMESTAMP,
  `whenDeleted` timestamp NULL default NULL,
  `sessionID` char(27) collate utf8_unicode_ci default NULL,
  `whenPasswordUpdated` timestamp NOT NULL default '0000-00-00 00:00:00',
  PRIMARY KEY  (`userId`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Dumping data for table `USERS`
--

INSERT INTO `USERS` (`userId`, `userName`, `password`, `accessLevel`, `firstName`, `lastName`, `email`, `whenLastLogin`, `whenCreated`, `whenDeleted`, `sessionID`, `whenPasswordUpdated`) VALUES
(1, 'LeeG134', '9���?�y������`���/u����k�i�l���o\rv�ngs��=�ݟyB�|O�_¨aV\Zš', 'admin', 'Lee XYXZ', 'LaAMESSS lulz', 'Lee@example.com', '2008-09-02 23:50:22', '2008-08-22 20:46:55', NULL, NULL, '2007-09-01 07:50:45'),
(2, 'BenG135', 'x\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 'viewer', 'first Name', 'last', 'x@y.net', '2008-08-31 13:30:31', '2008-08-27 13:13:09', NULL, NULL, '2008-08-22 20:46:55'),
(5, 'CatG3', '�e�x�g���P.l���H�D1��T�ˇ��i�<�iR���3PԻB�7�Uk�0�M�j�j廂.�', 'admin', 'testguy', 'somename', 'abc@def.com', '2008-08-31 13:30:31', '2008-08-27 14:55:10', NULL, NULL, '2008-08-22 20:46:55'),
(4, 'Deleted', 'G�m�����N�x�ϕL��Em�H�\04R$��P	�X�.P<Q��P�dc�t7��R���s�3', 'viewer', 'lol', 'cats', 'lolcats@example.com', '2008-08-31 13:30:31', '2008-08-27 14:05:07', '2008-08-31 14:24:23', NULL, '2008-08-22 20:46:55'),
(6, 'tu1', '��:y�An��L��JL��e��~I�#���P��;h�%2�p`�(kE%��G�C���6���|�x�', 'viewer', 'a', 'b', 'c@z.r', '2008-08-30 23:04:40', '2008-08-27 17:53:35', '2008-08-31 13:23:17', NULL, '2008-08-27 17:53:35'),
(18, 'kenyon', 'v����S(ɍVL߳JQr\\�\n�sk6`ADi��L1l/������w\r"�\r����w�֔���� �}~�', 'super', 'Kenyon', 'Ralph', 'kralph@ucsd.edu', '2008-09-05 00:44:12', '2008-08-31 17:58:26', NULL, '2SZ1RUmX1GgxYyyV1GNgP3TR8J3', '2008-09-05 00:21:46'),
(8, 'eedjit', '�"/���+Z�J�1g�+"�9��~$M��i�#Ƀ-����J7Z��;**���E�X`d��|', 'viewer', 'an', 'eedjit', 'fgsgfsdfg@sdfdf', '2008-08-30 23:04:40', '2008-08-28 01:21:19', '2008-08-31 14:23:22', NULL, '2008-08-28 01:21:19'),
(9, 'newtest', 'T3Pq�|�*�	?�.5�-8z:��;Y��hkʙA��W�W�����>��)�ˎ�T�C�7ѝB,�e', 'viewer', 'a', 'b', 'c@f.e', '2008-08-30 23:04:40', '2008-08-28 01:42:54', '2008-08-31 13:26:15', NULL, '2008-08-28 01:42:54'),
(10, 'joeblow', '¯qw	���''q�{��`i�!ab� Z<v�,j�L�-9�m5���k��g-9_��p�k�x�#', 'viewer', 'joe', 'blow', 'test@example.com', '2008-09-09 11:44:35', '2008-08-28 01:53:43', NULL, 'BsYk6q6F7vE-pfnEj5qqHZVZrxd', '2008-08-28 01:53:43'),
(12, 'zorg', '�:''��4�y�/\0V��u��:���Zq������u%4WP�0쥜���D������''B��', 'viewer', 'x', 'y', 'z@q.m', '2008-08-30 23:28:37', '2008-08-30 23:11:23', '2008-08-31 13:33:51', NULL, '2008-08-30 23:11:23'),
(11, 'borg', 'm~F�X\0��GA�zA���1Xy"���%{.��y�<?�����>���D_N�\Z��)^�Y	�)���R^`', 'super', 'Fst', 'Lst', 'FstLst@example.net', '2008-08-30 23:04:40', '2008-08-30 22:52:55', '2008-08-31 13:29:26', NULL, '2008-08-30 22:52:55'),
(13, 'DontDelete', '�.y���M�t72ppvڀD����>ɟ[Y	���[#��	�\r�*/���t�+�9���hVm�?', 'super', 'Dont Delete1', 'Dont Delete1', 'dontdel@email.com', '2008-09-05 11:07:05', '2008-08-31 14:05:29', NULL, 'zd3CP4Q8fpxSonFYluTf451IB-c', '2008-08-31 14:05:29'),
(14, 'DonDeleteTwo', '�Eaj���Q]�����O`��\\�?TH�lV�n��ګ�������R����0�*v��.�mU6�=�', 'super', 'DonDeleteTwo', 'DonDeleteTwo', 'DonDeleteTwo@email.com', '2008-08-31 17:45:39', '2008-08-31 15:13:18', NULL, NULL, '2008-08-31 15:13:18'),
(15, 'supertest', '�f�>��_�d�k''��l]=�Kvګ�v0����,����Țp�ѧ�_1���6�����6=�GTPt', 'admin', 'abc', 'def', 'zhangqiuyuan@gmail.com', '2008-09-10 17:24:31', '0000-00-00 00:00:00', NULL, '9VLPLaLXzN0-VmgPO8hYahCcg12', '2008-03-01 16:52:59'),
(16, 'johndoe', '��ç�\\�H���xZ�/;oB�_��ś���f��3������x�����''Y�b)�Bnk���[,', 'admin', 'john', 'doe', 'johndoe@aol.com', '2008-09-05 01:03:49', '2008-03-01 17:09:28', NULL, 'lclMy9c-n9cpRikyjxNvvC-sc0c', '2008-08-31 17:09:28'),
(20, 'an_hero!213', '�Y�eƖ����T߿_�\0�H�aO�3i�����Ԫ��?l��S��f���ݤ ���	�!W', 'viewer', 'AN', 'HERO', 'AN-HERO@nowhere.net', NULL, '2008-08-31 20:29:12', NULL, NULL, '2008-08-31 20:29:12'),
(17, 'vramirez', '6���������!��R�ke=�0�Q�g5PdE��I��%}5�\0��i���؊0lpu&��U�{�~', 'super', 'vianca', 'ramirez', 'asfds@asd.com', '2008-09-05 01:07:47', '2008-08-31 17:54:08', NULL, 'd88csz50E4HEWkecw50pT3mfrt6', '2008-08-31 17:54:08'),
(19, 'colorsadmin', '\nݰE��8���1����<�M�&���������h���#�tZ{�ž�b���ߍE���7��^\n�', 'admin', 'Admin', 'Tester', 'zorg@borg.org', '2008-09-05 00:26:08', '2008-08-31 18:48:34', NULL, NULL, '2008-08-31 18:48:34'),
(21, 'leeroy', '"�,.�9��ϽX�3vh��˾���(�+�mc�n���<pĔ��Z�sے!f���IRC�', 'viewer', 'leeroy', 'jenkins', 'lj@example.int', NULL, '2008-08-31 20:30:44', NULL, NULL, '2008-08-31 20:30:44'),
(22, 'super1', '�G��*��}NM���T��z�yZZU����cunit��oaY�_N4o�SwU�-ʏ�_#!+', 'super', 'superF', 'superLast', 'e@email.com', NULL, '2008-09-01 00:52:55', '2008-09-01 00:53:43', NULL, '2008-09-01 00:52:55'),
(23, 't-rex', '��%��i�S\Z���O��Q�\rU\nT����Cq������P�f� l:U���̻���`Hp�O���M', 'viewer', 'a', 'b', 'c@f.e', '2008-09-02 23:02:04', '2008-09-02 19:01:35', '2008-09-02 19:02:00', NULL, '2008-09-02 23:02:04'),
(25, 'boog', '4`S(F���)q-"f�.�g�\Z��yͪTUj^�ko7��K���0�&����7��x���*�O', 'viewer', 'xffauuy', 'yankerfeldge', 'x@y.net', '2008-09-03 23:39:11', '2008-09-03 12:57:29', NULL, NULL, '2008-09-03 23:38:59'),
(24, 'stupidhead', 'S�������6]X<Ȕ�=������VLFt�D�?%j�v�),���!/ѫ<.�N�h]\Z����X', 'viewer', 'a', 'b', 'c@f.e', NULL, '2008-09-03 00:04:55', '2008-09-03 00:05:02', NULL, '2008-09-03 00:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `USER_GROUP`
--
-- Creation: Aug 22, 2008 at 10:35 PM
-- Last update: Sep 01, 2008 at 06:53 AM
-- Last check: Aug 22, 2008 at 10:35 PM
--

DROP TABLE IF EXISTS `USER_GROUP`;
CREATE TABLE IF NOT EXISTS `USER_GROUP` (
  `userId` int(11) NOT NULL,
  `groupId` int(11) NOT NULL,
  KEY `userFK` (`userId`),
  KEY `groupFK` (`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `USER_GROUP`
--

INSERT INTO `USER_GROUP` (`userId`, `groupId`) VALUES
(1, 1),
(1, 4),
(1, 3),
(18, 14),
(2, 7),
(5, 6),
(19, 10),
(19, 11),
(19, 13),
(19, 15),
(19, 16),
(15, 1),
(15, 3),
(20, 1),
(20, 3),
(20, 4),
(21, 14),
(1, 5);
